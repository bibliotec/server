FROM python:3
ENV PYTHONUNBUFFERED=1
WORKDIR /vol/web
COPY requirements.txt /vol/web
RUN pip install -r requirements.txt
COPY . /vol/web
