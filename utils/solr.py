import os

from urllib.parse import urljoin

import requests


SOLR_HOST = os.environ.get('SOLR_HOST', 'localhost')
SOLR_PORT = os.environ.get('SOLR_PORT', 8983)
SOLR_URL  = 'http://{}:{}/solar'.format(SOLR_HOST, SOLR_HOST)

def get(url, params):
    params['wt'] = 'json'

    return requests.get(url, params=params).json()

def select(q):
    url = urljoin(SOLR_URL, 'select')

    payload = {
        'q': q
    }

    return get(url, params=payload)
