from rest_framework import permissions
from oauth2_provider.contrib.rest_framework import TokenMatchesOASRequirements


class IsOwnerOrAdmin(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object or admin to access it.
    """

    def has_object_permission(self, request, view, obj):
        return obj.owner == request.user or request.user.is_staff

