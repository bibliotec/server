from django.db import models
import pysolr

solr = pysolr.Solr(
    'http://localhost:8983/solr/',
    always_commit=True,
    timeout=10,)

# Create your models here.

class Author(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        abstract = True

class Category(models.Model):
    description = models.CharField(max_length=100)

    class Meta:
        abstract = True

class Tag(models.Model):
    description = models.CharField(max_length=100)

    class Meta:
        abstract = True

class Document(models.Model):
    title = models.CharField(max_length=100)
    mime_type = models.CharField(max_length=100)
    upload = models.FileField(upload_to='uploads/')
    content = models.TextField()
    author = models.ManyToManyField(Author)
    category = models.ManyToManyField(Category)
    tags = models.ManyToManyField(Tag)
    language = models.CharField(max_length=5)
    created_at = models.DateField(auto_now_add=True)
    archived_at = models.DateField()
    modified_at = models.DateField(auto_now=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        # solr.add([
        #     {
        #         "id": "doc_2",
        #         "title": "The Banana: Tasty or Dangerous?",
        #         "_doc": [
        #             { "id": "child_doc_1", "title": "peel" },
        #         ]
        #     },
        # ])
        pass

