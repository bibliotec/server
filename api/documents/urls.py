from django.urls import path

from .views import DocumentList, DocumentDetails


urlpatterns = [
    path('', DocumentList.as_view()),
    path('<pk>', DocumentDetails.as_view()),
]
