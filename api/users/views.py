from rest_framework import generics, permissions, serializers, viewsets, views, response
from utils.permissions import IsOwnerOrAdmin
from oauth2_provider.contrib.rest_framework import IsAuthenticatedOrTokenHasScope, TokenMatchesOASRequirements
from oauth2_provider.models import AccessToken

from .models import User


# first we define the serializers
# class UserSerializer(serializers.ModelSerializer):
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        owner = serializers.ReadOnlyField(source='owner.username')
        fields = ('username', 'email', 'first_name', 'last_name')

class UserViewSet(viewsets.ModelViewSet):
    '''
    API endpoint that allows users to be viewed or edited.
    '''
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    required_alternate_scopes = {
        'GET': [['read'], ['read:user']],
        'POST': [['create'], ['create:user']],
        'PUT':  [['update'], ['update:user']],
        'DELETE': [['delete'], ['delete:user']],
    }

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == 'list':
            permission_classes = [permissions.IsAdminUser, ]
        else:
            permission_classes = [TokenMatchesOASRequirements]

        return [permission() for permission in permission_classes]

class UserDetail(views.APIView):
    def get(self, request):
        token = request.headers.get('Authorization').split('Bearer')[1].strip()

        return response.Response(
            UserSerializer(AccessToken.objects.get(token=token).user).data)

