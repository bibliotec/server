from django.urls import path, include
from rest_framework import routers

from .views import UserViewSet, UserDetail

router = routers.DefaultRouter()
router.register(r'', UserViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('verify_credentials', UserDetail.as_view())
    # path('<pk>', UserDetails.as_view()),
]
