from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    owner = models.ForeignKey('self', on_delete=models.CASCADE)
