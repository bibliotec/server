# from django.db import models
from oauth2_provider.models import AbstractApplication

# Create your models here.
class Application(AbstractApplication):
    class Meta:
        db_table = 'oauth2_provider_application'
