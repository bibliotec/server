from rest_framework.views import APIView
from rest_framework import generics, serializers
from rest_framework.permissions import AllowAny

from .models import Application

class AppSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = [
            'id',
            'client_id',
            'client_secret',
            'redirect_uris',
            'name',
        ]
        read_only_fields = [
            'id',
            'client_id',
            'client_secret'
        ]

    def create(self, *args, **kwargs):
        args[0]['client_type'] = 'public'
        args[0]['authorization_grant_type'] = 'authorization-code'
        return super().create(*args, **kwargs)

class AppCreate(generics.CreateAPIView):
    queryset = Application
    serializer_class = AppSerializer
    permission_classes = [AllowAny]
