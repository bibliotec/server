from django.urls import path
from rest_framework import routers

from .views import AppCreate

urlpatterns = [
    path('', AppCreate.as_view()),
]
