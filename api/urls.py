from django.urls import path, include


urlpatterns = [
    path('users/', include('api.users.urls')),
    path('documents/', include('api.documents.urls')),
    path('apps/', include('api.apps.urls')),
]
