#!/bin/sh
# ./bin/solr create_core -c bibliotec & \

# Defining schema
url="http://${SOLR_HOST}:$SOLR_PORT/solr/bibliotec/schema"
echo $url
curl -X POST -H 'Content-type:application/json' --data-binary '
    {
        "add-field": [
            {
                "name":"title",
                "type":"text_general",
                "required":true
            },
            {
                "name":"path",
                "type":"string",
                "required":true,
                "indexed": false
            },
            {
                "name":"content",
                "type":"text_general",
                "required":true,
                "indexed": false
            },
            {
                "name":"author",
                "type":"text_general",
                "required":true,
                "multiValued":true
            },
            {
                "name":"category",
                "type":"string",
                "required":true,
                "multiValued":true
            },
            {
                "name":"tag",
                "type":"string",
                "required":true,
                "multiValued":true
            },
            {
                "name":"language",
                "type":"string",
                "required":true,
                "multiValued":true
            },
            {
                "name":"created_at",
                "type":"pdate",
                "required":true,
                "multiValued":true
            },
            {
                "name":"archived_at",
                "type":"pdate",
                "multiValued":true
            },
            {
                "name":"modified_at",
                "type":"pdate",
                "multiValued":true
            }
        ]
    }' $url
